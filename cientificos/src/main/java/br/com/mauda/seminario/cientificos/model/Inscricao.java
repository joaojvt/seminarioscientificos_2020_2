package br.com.mauda.seminario.cientificos.model;

import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao implements DataValidation {

    private Long id;
    private Boolean direitoMaterial;
    private SituacaoInscricaoEnum situacao = SituacaoInscricaoEnum.DISPONIVEL;
    private Estudante estudante;
    private Seminario seminario;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        seminario.adicionarInscricao(this);
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        this.situacao = SituacaoInscricaoEnum.COMPRADO;
        this.estudante = estudante;
        this.estudante.adicionarInscricao(this);
        this.direitoMaterial = direitoMaterial;

    }

    public void cancelarCompra() {
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
        this.estudante.removerInscricao(this);
        this.estudante = null;
        this.direitoMaterial = null;
    }

    public void realizarCheckIn() {
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Inscricao other = (Inscricao) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public void validateForDataModification() {
        if (this.getSituacao().equals(null)) {
            throw new SeminariosCientificosException("ER0040");
        }
        if (this.seminario == null) {
            throw new SeminariosCientificosException("ER0003");
        }
        this.seminario.validateForDataModification();

        if (!this.getSituacao().equals(SituacaoInscricaoEnum.DISPONIVEL)) {
        } else {
            if (this.direitoMaterial.equals(null)) {
                throw new SeminariosCientificosException("ER0041");
            }
            if (this.estudante.equals(null)) {
                throw new SeminariosCientificosException("ER0003");
            }
        }
        this.estudante.validateForDataModification();
    }

}
