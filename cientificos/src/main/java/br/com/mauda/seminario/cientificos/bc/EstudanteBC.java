package br.com.mauda.seminario.cientificos.bc;

import br.com.mauda.seminario.cientificos.model.DataValidation;

public class EstudanteBC extends PatternCrudBC<DataValidation> {

    private static EstudanteBC instance = new EstudanteBC();

    private EstudanteBC() {

    }

    public static EstudanteBC getInstance() {
        return instance;
    }
}
