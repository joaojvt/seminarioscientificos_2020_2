package br.com.mauda.seminario.cientificos.bc;

import br.com.mauda.seminario.cientificos.model.DataValidation;

public class AreaCientificaBC extends PatternCrudBC<DataValidation> {

    private static AreaCientificaBC instance = new AreaCientificaBC();

    private AreaCientificaBC() {

    }

    public static AreaCientificaBC getInstance() {
        return instance;
    }
}
