package br.com.mauda.seminario.cientificos.bc;

import br.com.mauda.seminario.cientificos.model.DataValidation;

public class CursoBC extends PatternCrudBC<DataValidation> {

    private static CursoBC instance = new CursoBC();

    private CursoBC() {

    }

    public static CursoBC getInstance() {
        return instance;
    }
}
