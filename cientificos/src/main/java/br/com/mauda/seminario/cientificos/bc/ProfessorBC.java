package br.com.mauda.seminario.cientificos.bc;

import br.com.mauda.seminario.cientificos.model.DataValidation;

public class ProfessorBC extends PatternCrudBC<DataValidation> {

    private static ProfessorBC instance = new ProfessorBC();

    private ProfessorBC() {

    }

    public static ProfessorBC getInstance() {
        return instance;
    }
}
