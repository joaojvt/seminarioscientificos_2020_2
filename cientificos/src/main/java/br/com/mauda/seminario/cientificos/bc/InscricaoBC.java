package br.com.mauda.seminario.cientificos.bc;

import java.util.Date;

import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.DataValidation;
import br.com.mauda.seminario.cientificos.model.Estudante;
import br.com.mauda.seminario.cientificos.model.Inscricao;
import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class InscricaoBC extends PatternCrudBC<DataValidation> {

    private static InscricaoBC instance = new InscricaoBC();
    Date dateAtual = new Date();

    private InscricaoBC() {

    }

    public static InscricaoBC getInstance() {
        return instance;
    }

    public void comprar(Inscricao inscricao, Estudante estudante, Boolean direitoMaterial) {
        if (inscricao.equals(null)) {
            throw new SeminariosCientificosException("ER0003");
        }
        if (direitoMaterial.equals(null)) {
            throw new SeminariosCientificosException("ER0041");
        }
        if (!inscricao.equals(SituacaoInscricaoEnum.DISPONIVEL)) {
            throw new SeminariosCientificosException("ER0042");
        }
        if (inscricao.getSeminario().getData().compareTo(this.dateAtual) < 0) {
            throw new SeminariosCientificosException("ER0043");
        }
        if (estudante.equals(null)) {
            throw new SeminariosCientificosException("ER0003");
        }
        estudante.validateForDataModification();
        inscricao.comprar(estudante, direitoMaterial);
    }

    public void cancelarCompra(Inscricao inscricao) {
        if (inscricao.equals(null)) {
            throw new SeminariosCientificosException("ER0003");
        }
        if (inscricao.getSeminario().getData().after(this.dateAtual)) {
            throw new SeminariosCientificosException("ER0044");
        }
        if (!inscricao.equals(SituacaoInscricaoEnum.COMPRADO)) {
            throw new SeminariosCientificosException("ER0045");
        }
        inscricao.cancelarCompra();

    }

    public void realizarCheckin(Inscricao inscricao) {
        if (inscricao.equals(null)) {
            throw new SeminariosCientificosException("ER0003");
        }
        if (!inscricao.equals(SituacaoInscricaoEnum.COMPRADO)) {
            throw new SeminariosCientificosException("ER0046");
        }
        if (inscricao.getSeminario().getData().after(this.dateAtual)) {
            throw new SeminariosCientificosException("ER0047");
        }
        inscricao.realizarCheckIn();
    }
}
