package br.com.mauda.seminario.cientificos.bc;

import br.com.mauda.seminario.cientificos.model.DataValidation;

public class InstituicaoBC extends PatternCrudBC<DataValidation> {

    private static InstituicaoBC instance = new InstituicaoBC();

    private InstituicaoBC() {

    }

    public static InstituicaoBC getInstance() {
        return instance;
    }

}
