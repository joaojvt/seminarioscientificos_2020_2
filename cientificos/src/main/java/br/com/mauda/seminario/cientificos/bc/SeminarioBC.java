package br.com.mauda.seminario.cientificos.bc;

import br.com.mauda.seminario.cientificos.model.DataValidation;

public class SeminarioBC extends PatternCrudBC<DataValidation> {

    private static SeminarioBC instance = new SeminarioBC();

    private SeminarioBC() {

    }

    public static SeminarioBC getInstance() {
        return instance;
    }
}
