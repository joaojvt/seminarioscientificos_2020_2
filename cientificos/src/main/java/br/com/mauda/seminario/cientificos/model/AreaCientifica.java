package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;

public class AreaCientifica implements DataValidation {

    private Long id;
    private String nome;
    private List<Curso> cursos = new ArrayList<>();

    public void adcionarCurso(Curso curso) {
        this.cursos.add(curso);
    }

    public Boolean possuiCurso(Curso curso) {
        return this.cursos.contains(curso);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Curso> getCursos() {
        return this.cursos;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        AreaCientifica other = (AreaCientifica) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public void validateForDataModification() {
        // verifica se a string não nula
        /*        if (this.nome == null){
            throw new SeminariosCientificosException("ER0010");
        }
        //verifica se a string está preenchida com espaços
        if (this.nome.isEmpty()) {
            throw new SeminariosCientificosException("ER0010");
        
        }
        //remove espaços em brancos contidos na string
        if (this.nome.trim().isEmpty()) {
            throw new SeminariosCientificosException("ER0010");
        }
        */
        // Método que substitui as anteriores
        if (StringUtils.isBlank(this.nome) || this.nome.length() > 50) {
            throw new SeminariosCientificosException("ER0010");
        }
    }
}
